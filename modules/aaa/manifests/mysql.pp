class aaa::mysql (
    $extracnf = hiera('aaa_extracnf'),
    $groupcnf = hiera('aaa_groupcnf'),
){

    define create_extra_config {
        file { "/etc/mysql/conf.d/${title}":
            ensure     => file, 
            source     => "puppet:///modules/aaa/etc/mysql/conf.d/${$title}",
            mode       => "0400", 
            owner      => "root",
        }
    }

    group { "mysql":
        ensure     => present,
        gid        => 3306,
    }
    ->
    user { "mysql":
        ensure     => present,
        gid        => 3306,
        shell      => "/bin/false",
        uid        => 3306,
    }
    ->
    file { ["/etc/mysql/", "/etc/mysql/conf.d"]:
        ensure     => directory,
        mode       => "0755"
    }
    ->
    file { "/etc/mysql/my.cnf":
        ensure     => file, 
        source     => "puppet:///modules/aaa/etc/mysql/my.cnf",
        mode       => "0400", 
        owner      => "root",
    }
    ->
    file { "/etc/mysql/conf.d/group.cnf":
        ensure     => file,
        content    => template("aaa/mysql/group.cnf.erb"),
        mode       => "0400",
        owner      => "root",
    }
    ->
    file { "/etc/mysql/conf.d/node.cnf":
        ensure     => file,
        content    => template("aaa/mysql/node.cnf.erb"),
        mode       => "0400",
        owner      => "root",
    }
    ->
    file { ["/var/lib/mysql/", "/var/lib/mysql/bin-logs/", "/var/lib/mysql/data/", "/var/lib/mysql/innodb/", "/var/lib/mysql/relay-logs/"]:
        ensure     => directory,
        owner      => "mysql",
        group      => "mysql",
        mode       => "0700",
    }
    ->
    create_extra_config { $extracnf: }

}