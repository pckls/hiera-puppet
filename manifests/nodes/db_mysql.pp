node "testdbm1.example.org" {

    $role = 'db'
    $technology = 'mysql'
    $groupname = regsubst($fqdn, '\d+', '')
    $nodenumber = regsubst($hostname, '[a-z,-]+', '')

    ## Useful for domain specific config (monitoring etc...) 
    case $domain {
        'example1.org': { $somerelevantip = "10.1.1.1" }
        'example2.org': { $somerelevantip = "10.1.2.1" }
        default:        { $somerelevantip = "10.1.3.1" }
    }
    
    include aaa
    include aaa::mysql
    include lvm
    include mysql::server

    Class['aaa'] -> Class['lvm'] -> Class['aaa::mysql'] -> Class['mysql::server']
    
}