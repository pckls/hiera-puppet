##
## This manifest is used by Vagrant.
##

include aaa::vagrant
include apt
include motd

Class['apt'] -> Class['aaa::vagrant'] -> Class['motd']