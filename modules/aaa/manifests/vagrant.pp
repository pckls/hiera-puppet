class aaa::vagrant (
    $packagename = "hiera-puppet",
){

  file { "/etc/puppet/manifests":
    ensure => link,
    target => "/vagrant/${packagename}/manifests",
    force => true,
    replace => true,
    owner => "root",
    mode => "0700",
  }

  file { "/etc/puppet/modules":
    ensure => link,
    target => "/vagrant/${packagename}/modules",
    force => true,
    replace => true,
    owner => "root",
    mode => "0700",
  }

  file { "/etc/puppet/hieradata":
    ensure => link,
    target => "/vagrant/${packagename}/hieradata",
    force => true,
    replace => true,
    owner => "root",
    mode => "0700",
  }

  file { "/etc/puppet/hiera.yaml":
    ensure => link,
    target => "/vagrant/${packagename}/hiera.yaml",
    force => true,
    replace => true,
    owner => "root",
    mode => "0700",
  }

  file { "/etc/hiera.yaml":
    ensure => link,
    target => "/vagrant/${packagename}/hiera.yaml",
    force => true,
    replace => true,
    owner => "root",
    mode => "0700",
  }

}
