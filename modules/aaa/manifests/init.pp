class aaa (
    $basepackages = {},
){
    define install_base_packages {
        package { "${title}":
            ensure => installed,
        }
    }

    install_base_packages { $basepackages: }
    
}
