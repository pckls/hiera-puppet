node "testdbm1.example.org" {

    $role = "db"
    $technology = "postgresql"
    $groupname = regsubst($fqdn, '\d+', '')
    $nodenumber = regsubst($hostname, '[a-z,-]+', '')
    
    include aaa
    include lvm
    include postgresql::server

    Class['aaa'] -> Class['lvm'] -> Class['postgresql::server']
    
}