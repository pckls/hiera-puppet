hiera-puppet
============
Just another ~~wordpress~~ repo of puppet manifests with some hiera and vagrant thrown in for flavour.

Motivation
----------

I was recently tasked with automating aspects of MySQL server deployments. After many iterations I ended up with something that I was happy enough with to use for production deployments. This repo is the next iteration and is designed to be more generic and publicly available.

The basic idea is to use the fqdn fact to determine the node classification and then define roles/technologies. I feel that this hierachy works quite well for a mix of roles and technologies and *should* scale well. So far I have implemented a couple of technologies within the Database role (MySQL & PostgreSQL) but I plan to extend this repo to provide basic builds of various Web and Application technologies as well.

Installation
--------------

Assuming you already have Vagrant installed, do the following to bring up a MySQL server:

```sh
git clone [git-repo-url]
ln -s hiera-puppet/vagrant/Vagrantfile.MySQL Vagrantfile
vagrant up
```

You should now have:

 - an Ubuntu 14.04 VM
 - an additional disk, partitioned, LVM'ed and formatted
 - the Percona MySQL repo added as an apt source
 - standard mysql-server installed, data residing on the LV
 - some additional packages that I enjoy

It should be relatively simple to experiment with additional variables in Hiera.

TODO
----

 - More documentation
 - Implement further roles and technologies (starting with apache/ngnix)
 - Further refine the existing database technologies