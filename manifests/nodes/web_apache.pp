node "www1.example.org" {

    $role = "web"
    $technology = "apache"
    $groupname = regsubst($fqdn, '\d+', '')
    
    include aaa
    include aaa::apache
    include apache
    include lvm

    Class['aaa'] -> Class['lvm'] -> Class['apache']

}