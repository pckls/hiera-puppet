node "ntp1.example.org" {

    $role = "system"
    $technology = "ntp"
    $groupname = regsubst($fqdn, '\d+', '')
    
    include aaa
    include ntp

}