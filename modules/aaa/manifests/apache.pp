class aaa::apache (
    $hosts = {},
    $serverip = '127.0.1.1',
    $vhosts = hiera('aaa::apache::vhosts', {}),
){
    create_resources('apache::vhost', $vhosts)
    
    host { $hostname:
        ip => $serverip,
        host_aliases => $hosts,
    }
}